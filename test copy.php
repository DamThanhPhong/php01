<?php

    // 定義
    const PLUS     = '1';
    const MINUS    = '2';
    const MULTI    = '3';
    const DIVISION = '4';

    const OPERATOR = [
        '＋' => PLUS,
        '+'  => PLUS,
        'ー' => MINUS,
        '-'  => MINUS,
        '＊' => MULTI,
        '*'  => MULTI,
        '×'  => MULTI,
        '÷'  => DIVISION,
        '／' => DIVISION,
        '/'  => DIVISION,
    ];

    /**
     * 渡された演算子と２つの数字をもとに計算する
     * 渡された演算子が判断できない場合は $value1 を返却する
     * @param $operator 演算子
     * @param $value1   数字１
     * @param $value2   数字２
     */
    function calculate($operatorArg, $value1, $value2) {
        
        // 計算結果にデフォルトで $value1 を設定する
        $result = $value1;

        // 演算子を1~4の数字に変換する
        $operator = OPERATOR[$operatorArg];

        // 1~4に変換された演算子を判断して $value1 と $value2 を計算する
        switch ($operator) {
            case PLUS:
                $result = $value1 + $value2;
                break; 
            case MINUS:
                $result = $value1 - $value2;
                break;
            case MULTI:
                $result = $value1 * $value2;
                break;
            case DIVISION:
                $result = $value1 / $value2;
                break;
            default:
                break;
        }
        return $result;
    }
    
    
    // 入力
    $inputValues = [1, '+', 2, '-', 3, '+', 2, '*', 5];
    //$inputValues = [1, 2, 3, 2, 5];
    //$operators = ['+', '-', '+', '*'];

    $res=0; // 結果
    $ope=null; // 演算子
    $val=0; // 数字（$values2に相当）

    //計算

    //$iは$inputValuesのインデックス番号
    for($i=0 ; $i<count($inputValues)-2; $i=$i+2){
        //$inputValues[0]の値を受け取り、表示
        if(!$i){
            $res=$inputValues[$i];
            echo "最初の過程：". "\n";
            echo "res：" . $res . "\n";
        }
        $ope=$inputValues[$i+1];            //$opeを定義
        $val=$inputValues[$i+2];            //$valを定義
        $res=calculate($ope,$res,$val);     //calculate関数を利用し、$resを計算
        //計算過程の詳細を表示
        echo "----------\n";
        echo "過程：". "\n";
        echo "ope：" . $ope . "\n";
        echo "val：" . $val . "\n";
        echo "res：" . $res . "\n";
    }
    //結果表示
    echo "----------\n";
    echo '結果:'.$res;
?>