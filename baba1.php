<?php
/*前提
・トランプの数：
　　ハート、スペード、ダイヤ、クローバー、それぞれ13枚（計52枚）
　　ジョーカー1枚
　　※ 全53枚
・プライヤー：2人

処理
１．プライヤー2人にランダムでトランプを振り分ける
２．各プレイヤーはトランプのペア（同じ数字が2枚）を捨てる
３．プレイヤー➀はプレイヤー②のカードを引く
４．プレイヤー➀はペアをチェックし、ペアがあれば捨てる。
５．ペアがなければプレイヤー②が引く
６．「３～５」を繰り返す
７．いずれかのプレイヤーのトランプがなくなったら終了*/



//１．プライヤー2人にランダムでトランプを振り分ける

//53枚のトランプを準備する。
//'j'をjokerとし、ハート、スペード、ダイヤ、クローバー、それぞれ13枚を1a,...13dというふうに考え、配列$trumpsを作る
$trumps=['j','1a','2a','3a','4a','5a','6a','7a','8a','9a','10a','11a','12a','13a','1b','2b','3b','4b','5b','6b','7b','8b','9b','10b','11b','12b','13b','1c','2c','3c','4c','5c','6c','7c','8c','9c','10c','11c','12c','13c','1d','2d','3d','4d','5d','6d','7d','8d','9d','10d','11d','12d','13d'];

// 配列をシャッフルする
shuffle($trumps);

//シャッフルした配列を見てみる
//print_r($trumps);

//$player1s、$player2sを２人の参加者の振り分けられたトランプの値の配列
$player1s= [] ;
$player2s=[];

//一人目に２７枚を振り分ける
for($i=0;$i<27;$i++){
	$player1s[]=$trumps[$i] ;
}
//print_r($player1s);

//二人目に２６枚を振り分ける。
for($i=27;$i<53;$i++){
	$player2s[]=$trumps[$i] ;
}
//print_r($player2s);
//echo $player2s[0];



//２．各プレイヤーはトランプのペア（同じ数字が2枚）を捨てる
//https://qiita.com/Pell/items/def5860ccdef1ccdfe1c のjavascriptコードを参考に書く
for ($baseIdx = 0; $baseIdx < count($player1s); $baseIdx++) {
	for ($targetIdx = 0; $targetIdx < count($player1s); $targetIdx++) {
		if (($targetIdx != $baseIdx) &&
			($player1s[$targetIdx][0] == $player1s[$baseIdx][0])) {
			if ($targetIdx < $baseIdx) {
				array_splice($player1s,$baseIdx, 1);
				array_splice($player1s,$targetIdx, 1);
			} else {
				array_splice($player1s,$targetIdx, 1);
				array_splice($player1s,$baseIdx, 1);
			}
			$baseIdx--;
			break;
		}
	}
}
print_r($player1s);

for ($baseIdx = 0; $baseIdx < count($player2s); $baseIdx++) {
	for ($targetIdx = 0; $targetIdx < count($player2s); $targetIdx++) {
		if (($targetIdx != $baseIdx) &&
			($player2s[$targetIdx][0] == $player2s[$baseIdx][0])) {
			if ($targetIdx < $baseIdx) {
				array_splice($player2s,$baseIdx, 1);
				array_splice($player2s,$targetIdx, 1);
			} else {
				array_splice($player2s,$targetIdx, 1);
				array_splice($player2s,$baseIdx, 1);
			}
			$baseIdx--;
			break;
		}
	}
}
print_r($player2s);

/*ペアをチェックする関数を作る
function checkPair($players){
	for ($baseIdx = 0; $baseIdx < count($players); $baseIdx++) {
		for ($targetIdx = 0; $targetIdx < count($players); $targetIdx++) {
			if (($targetIdx != $baseIdx) &&
				($players[$targetIdx][0] == $players[$baseIdx][0])) {
				if ($targetIdx < $baseIdx) {
					array_splice($players,$baseIdx, 1);
					array_splice($players,$targetIdx, 1);
				} else {
					array_splice($players,$targetIdx, 1);
					array_splice($players,$baseIdx, 1);
				}
				$baseIdx--;
				break;
			}
		}
	}
	print_r($players);
}
checkPair($player2s);*/
//３．プレイヤー➀はプレイヤー②のカードを引く

//ランダムで追加トランプを決める
function addTo1($player1s,$player2s){
	$random1=rand(0,count($player2s)-1);
	$player1s[]=$player2s[$random1];
	print_r($player1s);
}
addTo1($player1s,$player2s);


//４．プレイヤー➀はペアをチェックし、ペアがあれば捨てる。
//（２.の処理をする必要があります）

//５．ペアがなければプレイヤー②が引く
function addTo2($player2s,$player1s){
	$random2=rand(0,count($player1s)-1);
	$player2s[]=$player1s[$random2];
	print_r($player2s);
}
addTo2($player2s,$player1s);
//６．「３～５」を繰り返す



//７．いずれかのプレイヤーのトランプがなくなったら終了

if(count($player1s)==0){
	echo "プレイヤー１が勝ちました";
}
if(count($player2s)==0){
	echo "プレイヤー２が勝ちました";
}


?>