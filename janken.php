<?php
//今回のじゃんけんゲームでじゃんけんする回数を1回のみとします。

//今回のゲームで自分を$member1とし、相手を$member2とします。

//じゃんけんゲームでの指の出し方（"グー", "チョキ", "パー"）を配列$jankenに入れる
$janken = array("グー", "チョキ", "パー");

//$member1の計算で使う$jankenのインデックス番号をランダムで決める。
$janken_key1 = rand(0,2);
//$member2の計算で使う$jankenのインデックス番号をランダムで決める。
$janken_key2 = rand(0,2);

//member1とmember2の値は$jankenからランダムで決めたインデックス番号で決まる。
$member1= $janken[$janken_key1] . "\n";
$member2= $janken[$janken_key2] . "\n";

//自分と相手の指の出し方（"グー", "チョキ", "パー"）
echo '自分:'.$member1;
echo '相手:'.$member2;

// 勝敗を判定
if ($member1==$member2 ) {
    $result ='引き分け';
} elseif (  $member1== 'グー' && $member2  == 'チョキ') {
    $result = '自分が勝ち';
} elseif ($member1 == 'チョキ' && $member2 == 'パー') {
    $result = '自分が勝ち';
} elseif ($member1 == 'パー' && $member2 == 'グー') {
    $result = '自分が勝ち';
} else {
    $result = '自分が負け';
}

//結果を表示
echo "----------\n";
echo '結果:'.$result;

?>